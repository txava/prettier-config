
## Description

Prettier rules for my personal projects.
Meant to be used as part of [@txava/eslint-config](https://gitlab.com/txava/eslint-config)

## Installation


```sh
yarn add -D @txava/prettier-config prettier@~2.2.1
```
