# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/txava/prettier-config/compare/v1.0.0...v1.0.1) (2021-02-06)


### Bug Fixes

* add missing rules file ([ebf20c1](https://gitlab.com/txava/prettier-config/commit/ebf20c157136db8abf77654d07b4573987945876))

## 1.0.0 (2021-02-06)


### Features

* add rules ([ef92ba0](https://gitlab.com/txava/prettier-config/commit/ef92ba07c3b87309f2d9a8cec4c16c929fb27418))
